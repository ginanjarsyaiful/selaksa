;(function ($) {
    'use strict';

    function animationChild() {
        if($(".animation-content[data-css-animation]").length){
            $(".animation-content[data-css-animation]").each(function () {
                var element = $(this),
                    duration = parseInt(element.attr('data-duration')),
                    delay = parseInt(element.attr('data-delay')),
                    animation = element.attr('data-css-animation');

                if ($(this).children().not('br').length) {
                    $(this).children().not('br').each(function () {
                        $(this)
                            .attr('data-css-animation', animation);

                        if (!isNaN(duration)) {
                            $(this)
                                .attr('data-duration', duration);
                        }

                        if (!isNaN(delay)) {
                            $(this)
                                .attr('data-delay', delay);
                        }

                        delay += 100;
                    });

                    element.removeAttr('data-duration data-delay data-css-animation');
                } else {
                    element.removeClass('animation-content');
                }
            });
        }

    }

    function animation() {
        if($("[data-css-animation]:not(:animated)").length){
            $("[data-css-animation]:not(:animated)").waypoint(function(){
                var element = $(this);
                var duration = parseInt(element.attr('data-duration')),
                    delay = parseInt(element.attr('data-delay')),
                    animation = element.attr('data-css-animation');


                if (!isNaN(duration)) {
                    element
                        .css('animation-duration', duration + 'ms')
                        .css('transition-duration', duration + 'ms')
                }

                element
                    .addClass(animation);

                if (element.hasClass('swiper-slide') && element.hasClass('swiper-slide-visible') || !element.hasClass('swiper-slide')) {
                    if (!isNaN(delay)) {
                        element
                            .css('animation-delay', delay + 'ms')
                            .css('transition-delay', delay + 'ms')
                    }

                    element
                        .addClass("animated");
                }

            }, {
                offset: "85%"
            });
        }

    }



    function animationGrid() {
        if($(".grid-item-fade-up").length) {
            $(".grid-item-fade-up").waypoint(function () {

                $(this)
                    .addClass('animated');

            }, {
                offset: "85%"
            });
        }
    }

    $(window).on('load', function () {
        animationChild();
        setTimeout(animation, 500);
        animationGrid();
    });

    $(window).on('scroll', function () {
        animationChild();
        animation();
        animationGrid();
    });

})(jQuery, window, document);