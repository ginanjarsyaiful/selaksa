;(function ($, window, document, undefined) {
    'use strict';

    $(window).on('load', function () {
        if ($('.main-preloader.active').length) {
            $('.main-preloader.active').fadeOut(500);
        }
        if ($('.main-preloader:not(.active)').length) {
            $('.main-preloader').fadeOut(400);
        }

    });

    /*=================================*/
    /* 01 - VARIABLES */
    /*=================================*/
    var swipers = [];

    $('body').fitVids({ignore: '.vimeo-video, .youtube-simple-wrap iframe, .iframe-video.for-btn iframe, .post-media.video-container iframe'});
    /*=================================*/
    /* 02 - PAGE CALCULATIONS */
    /*=================================*/
    /**
     *
     * PageCalculations function
     * @since 1.0.0
     * @version 1.0.1
     * @var winW
     * @var winH
     * @var winS
     * @var pageCalculations
     * @var onEvent
     **/
    if (typeof pageCalculations !== 'function') {

        var winW, winH, winS, pageCalculations, onEvent = window.addEventListener;

        pageCalculations = function (func) {

            winW = window.innerWidth;
            winH = window.innerHeight;
            winS = document.body.scrollTop;

            if (!func) return;

            onEvent('load', func, true); // window onload
            onEvent('resize', func, true); // window resize
            onEvent("orientationchange", func, false); // window orientationchange

        };

        pageCalculations(function () {
            pageCalculations();
        });
    }

    /*Full height banner*/
    function topBannerHeight() {
        var headerH = $('.header_top_bg').not('.header_trans-fixed, .fixed-header').outerHeight() || 0;
        var windowH = $(window).height();
        var footerH = $('#footer').outerHeight();
        var offsetTop;
        if ($('#wpadminbar').length) {
            offsetTop = headerH + $('#wpadminbar').outerHeight();
        } else {
            offsetTop = headerH;
        }
        $('.full-height-window').css('min-height', (windowH - offsetTop) + 'px');
        $('.full-height').css('min-height', (windowH - offsetTop - footerH ) + 'px');

    }

    // VIDEO BUTTON

    $(window).on('load', function () {
        if($('.video-button').length) {
            $('.video-button').each(function() {
                $(this).magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: true,
                    fixedBgPos: true
                });
            });
        }
    });


    /* IF TOUCH DEVICE */
    function isTouchDevice() {
        return 'ontouchstart' in document.documentElement;
    }

    /*=================================*/
    /* SWIPER SLIDER */

    /*=================================*/
    function initSwiper() {
        var initIterator = 0;

        $('.swiper-container:not(.post-slider):not(.img-slider):not(.team-info-section):not(.team-img-section):not(.test-info-section):not(.test-img-section):not(.speakers):not(.modern-info):not(.modern-img)').each(function () {
            var $t = $(this);

            var index = 'swiper-unique-id-' + initIterator;
            $t.addClass('swiper-' + index + ' initialized').attr('id', index);
            $t.parent().find('.swiper-pagination').addClass('swiper-pagination-' + index);

            if($t.hasClass('.outer-pagination')){
                $t.closest('.swiper-container-wrap').find('.swiper-pagination').addClass('swiper-pagination-' + index);
            }else{
                $t.parent().find('.swiper-pagination').addClass('swiper-pagination-' + index);
            }

            $t.parent().find('.swiper-button-next').addClass('swiper-button-next-' + index);
            $t.parent().find('.swiper-button-prev').addClass('swiper-button-prev-' + index);

            if (isTouchDevice() && $t.data('mode') == 'vertical') {
                $t.attr('data-noswiping', 1);
                $(this).find('.swiper-slide').addClass('swiper-no-swiping');
            }

            var autoPlayVar = parseInt($t.attr('data-autoplay'), 10);
            var mode = $t.attr('data-mode') ? $t.attr('data-mode') : 'horizontal';
            var effect = $t.attr('data-effect') ? $t.attr('data-effect') : 'slide';
            var paginationType = $t.attr('data-pagination-type') ? $t.attr('data-pagination-type') : 'bullets';
            var loopVar = parseInt($t.attr('data-loop'), 10) ? parseInt($t.attr('data-loop'), 10) : false;
            var noSwipingVar = parseInt($t.attr('data-noSwiping'), 10) ? parseInt($t.attr('data-noSwiping'), 10) : true;
            var mouse = parseInt($t.attr('data-mouse'), 10) ? parseInt($t.attr('data-mouse'), 10) : false;
            var speedVar = parseInt($t.attr('data-speed'), 10) ? parseInt($t.attr('data-speed'), 10) : '1500';
            var centerVar = parseInt($t.attr('data-center'), 10) ? parseInt($t.attr('data-center'), 10) : false;
            var spaceBetweenVar = parseInt($t.attr('data-space'), 10) ? parseInt($t.attr('data-space'), 10) : 0;
            var slidesPerView = parseInt($t.attr('data-slidesPerView'), 10) ? parseInt($t.attr('data-slidesPerView'), 10) : 'auto';
            var slidesPerColumn = parseInt($t.attr('data-slidesPerColumn'), 10) ? parseInt($t.attr('data-slidesPerColumn'), 10) : '1';
            var heightVar = parseInt($t.attr('data-height'), 10) ? parseInt($t.attr('data-height'), 10) : false;
            var breakpoints = {};
            var responsive = parseInt($t.attr('data-responsive'), 10) ? parseInt($t.attr('data-responsive'), 10)  : false;
            var loopedSlides = slidesPerView === 'auto' ? 5 : null;

            if (responsive) {

                slidesPerView = $t.attr('data-add-slides');
                var lg = $t.attr('data-lg-slides') ? $t.attr('data-lg-slides') : $t.attr('data-add-slides');
                var md = $t.attr('data-md-slides') ? $t.attr('data-md-slides') : $t.attr('data-add-slides');
                var sm = $t.attr('data-sm-slides') ? $t.attr('data-sm-slides') : $t.attr('data-add-slides');
                var xs = $t.attr('data-xs-slides') ? $t.attr('data-xs-slides') : $t.attr('data-add-slides');

                var spaceLg = $t.attr('data-lg-space') ? $t.attr('data-lg-space') : $t.attr('data-space');
                var spaceMd = $t.attr('data-md-space') ? $t.attr('data-md-space') : $t.attr('data-space');
                var spaceSm = $t.attr('data-sm-space') ? $t.attr('data-sm-space') : $t.attr('data-space');
                var spaceXs = $t.attr('data-xs-space') ? $t.attr('data-xs-space') : $t.attr('data-space');


                breakpoints = {
                    600: {
                        slidesPerView: parseInt(xs, 10),
                        spaceBetween: parseInt(spaceXs, 10)
                    },
                    992: {
                        slidesPerView: parseInt(sm, 10),
                        spaceBetween: parseInt(spaceSm, 10)
                    },
                    1199: {
                        slidesPerView: parseInt(md, 10),
                        spaceBetween: parseInt(spaceMd, 10)
                    },
                    1600: {
                        slidesPerView: parseInt(lg, 10),
                        spaceBetween: parseInt(spaceLg, 10),
                    }
                };

            }

            if($t.hasClass('test-vert')){
                breakpoints = {
                    1200: {
                        slidesPerView: 1,
                        direction: 'horizontal',
                        noSwiping: 0
                    },
                };
            }

            swipers['swiper-' + index] = new Swiper('.swiper-' + index, {
                pagination: {
                    el: '.swiper-pagination-' + index,
                    clickable: true,
                    type: paginationType,
                    renderBullet: function (index, className) {
                        if($t.hasClass('outer-pagination')){
                            return '<span class="' + className + '">' + (index + 1) + '</span>';
                        }else{
                            return '<span class="' + className + '"></span>';
                        }
                    },
                    renderProgressbar: function (progressbarFillClass) {
                        return '<span class="' + progressbarFillClass + '"></span>';
                    },
                },
                navigation: {
                    nextEl: '.swiper-button-next-' + index,
                    prevEl: '.swiper-button-prev-' + index,
                },
                direction: mode || 'horizontal',
                slidesPerView: slidesPerView,
                slidesPerColumn: slidesPerColumn,
                breakpoints: breakpoints,
                centeredSlides: centerVar,
                noSwiping: noSwipingVar,
                noSwipingClass: 'swiper-no-swiping',
                spaceBetween: spaceBetweenVar,
                loop: loopVar,
                speed: speedVar,
                loopedSlides: loopedSlides,
                autoplay: {
                    delay: autoPlayVar,
                },
                effect: effect,
                mousewheel: mouse,
                lazy: true,
                iOSEdgeSwipeDetection: true,
                autoHeight: heightVar,
                watchSlidesVisibility: true,
                on: {
                    init: function(){
                        if($t.closest('.info-block-wrap.slider').length){
                            var current = +$t.find('.swiper-slide-active').attr('data-swiper-slide-index') + 1;
                            var total = $t.find('.swiper-slide:not(.swiper-slide-duplicate)').length;

                            current = current < 10 ? '0' + current : current;
                            total = total < 10 ? '0' + total : total;

                            $t.find('.swiper-number-pagination .current').text(current);
                            $t.find('.swiper-number-pagination .total').text(total);

                        }
                    },
                    slideChangeTransitionStart: function () {

                        $t.find('.swiper-slide-visible .skill').each(function () {

                            var procent = $(this).attr('data-value');
                            var circle = $(this).find('svg .bar');
                            var r = circle.attr('r');
                            var c = Math.PI*(r*2);
                            if ( procent < 0 ) {
                                procent = 0;
                            }
                            if ( procent > 100 ) {
                                procent = 100;
                            }
                            var pct = ((100 - procent) / 100)*c;
                            circle.css({strokeDashoffset: pct});
                        });

                        if($t.closest('.info-block-wrap.slider').length){
                            var current = +$t.find('.swiper-slide-active').attr('data-swiper-slide-index') + 1;
                            var total = $t.find('.swiper-slide:not(.swiper-slide-duplicate)').length;

                            current = current < 10 ? '0' + current : current;
                            total = total < 10 ? '0' + total : total;

                            $t.find('.swiper-number-pagination .current').text(current);
                            $t.find('.swiper-number-pagination .total').text(total);

                        }

                    },
                    slideChange: function () {
                        if ($t.find('.swiper-slide-visible[data-css-animation]:not(.animated)').length && ($t.offset().top < $(window).scrollTop() + $(window).height())) {
                            var delay = 300;
                            $t.find('.swiper-slide-visible[data-css-animation]:not(.animated)').each(function () {
                                $(this)
                                    .css('animation-delay', delay + 'ms')
                                    .css('transition-delay', delay + 'ms').addClass('animated');

                                delay += 300;
                            });
                        }

                    },
                    slideChangeTransitionEnd: function () {
                        if($t.hasClass('carousel-speakers')){
                            $t.find('.swiper-slide').each(function() {
                                $(this).removeClass('animation-slide-on');
                            });
                            $t.find('.swiper-slide-active').addClass('animation-slide-on');
                        }
                    },
                },
            });

            initIterator++;
        });
    }


    // COLUMN DELAY ANIMATION
    function delayColumn() {
        if ( $('.wpb_column').length ) {
            $('.wpb_column').each(function() {
                if ( $(this).data('delay') != null  ) {
                    var dataDelay = $(this).data('delay');
                    $(this).css('animation-delay', dataDelay + 'ms' );
                }
            });
        }
    }


    /* One page menu */
    if ($('.vc_row').length && $('.vc_row').attr('id')) {
        $(window).on('scroll', function () {
            var wintop = $(this).scrollTop();
            $('.vc_row').each(function () {
                var $this = $(this);
                if (wintop > $(this).offset().top - ($(window).height() / 1.3)) {
                    var currentId = $this.attr('id');
                    var reqLink = $('ul.menu li:not(.menu-item-has-children) > a').filter('[href="#' + currentId + '"]');
                    reqLink.closest('li:not(.menu-item-has-children)').addClass('active').siblings().removeClass('active');
                }
            });
        });
    }

    if($(window).width() >= $('.main-wrapper').data('top')) {
        $('ul.menu li:not(.menu-item-has-children) > a[href^="#"]').on('click', function () {
            var elem = $(this).attr('href');
            $('html,body').animate({
                    scrollTop: $(elem).offset().top - 50},
                'slow');
        });

    }

    /*end of one page menu*/


    /*=================================*/
    /* MAIN WRAPPER */
    /*=================================*/

    function calcPaddingMainWrapper() {
        var footer = $('#footer');
        var paddValue = footer.outerHeight();
        footer.bind('heightChange', function () {
            if (!$("#footer.fix-bottom").length && $("#footer.footer-parallax").length) {
                $('.main-wrapper').css('margin-bottom', paddValue);
            } else if (!$("#footer.fix-bottom").length) {
                $('.main-wrapper').css('padding-bottom', paddValue);
            }
        });

        footer.trigger('heightChange');
    }

    $(window).on('load', function () {
        delayColumn();
    });


    /*=================================*/
    /* BLOG */
    /*=================================*/

    /* MAGNIFIC POPUP VIDEO */
    function popupVideo() {
        if ($('.blog .video-content-blog').length || $('.single-post .video-content-blog').length || $('.post-list-wrapper.fullscreen .video-content-blog').length ) {
            $('.play').each(function () {
                $(this).magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: true,
                    fixedBgPos: true
                });
            });
        }
    }

    // isotope
    function initBlogIsotope() {
        if ($('.izotope-blog').length) {
            var self = $('.izotope-blog');
            var layoutM = 'masonry';
            self.isotope({
                itemSelector: '.post',
                layoutMode: layoutM,
                masonry: {
                    columnWidth: '.post'
                }
            });
        }
    }


    /*=================================*/
    /* BLOG IMAGE SLIDER */
    /*=================================*/

    function blogImageSlider() {
        if ($('.img-slider').length) {
            $('.img-slider .slides').not('.slick-initialized').each(function () {
                $(this).slick({
                    fade: true,
                    autoplay: true,
                    speed: 500,
                    dots: false,
                    prevArrow: "<div class='flex-prev'><i class='ion-arrow-left-c'></i></div>",
                    nextArrow: "<div class='flex-next'><i class='ion-arrow-right-c'></i></div>"
                });
            })
        }
    }


    /*=================================*/
    /* ADD IMAGE ON BACKGROUND */
    /*=================================*/

    function wpc_add_img_bg(img_sel, parent_sel) {
        if (!img_sel) {

            return false;
        }
        var $parent, $imgDataHidden, _this;
        $(img_sel).each(function () {
            _this = $(this);
            $imgDataHidden = _this.data('s-hidden');
            $parent = _this.closest(parent_sel);
            $parent = $parent.length ? $parent : _this.parent();
            $parent.css('background-image', 'url(' + this.src + ')').addClass('s-back-switch');
            if ($imgDataHidden) {
                _this.css('visibility', 'hidden');
                _this.show();
            }
            else {
                _this.hide();
            }
        });
    }


    /*=================================*/
    /* MOBILE MENU */
    /*=================================*/

    $('.mob-nav').on('click', function (e) {
        e.preventDefault();
        $('html').addClass('no-scroll sidebar-open').height(window.innerHeight + 'px');
    });
    $('.mob-nav-close').on('click', function (e) {
        e.preventDefault();
        $('html').removeClass('no-scroll sidebar-open').height('auto');
    });


    function fixedMobileMenu() {
        var headerHeight = $('.header_top_bg').not('.header_trans-fixed').outerHeight();
        var offsetTop;
        var dataTop = $('.main-wrapper').data('top');
        var adminbarHeight = $('#wpadminbar').outerHeight();
        if ($('#wpadminbar').length) {
            offsetTop = adminbarHeight + headerHeight;
            $('.header_top_bg').css('margin-top', adminbarHeight);
        } else {
            offsetTop = headerHeight;
        }
        if ($(window).width() < dataTop) {
            $('.main-wrapper').css('padding-top', offsetTop + 'px');
        } else {
            if ($('#wpadminbar').length && $('.header_top_bg').hasClass('header_trans-fixed')) {
                $('.main-wrapper').css('padding-top', adminbarHeight + 'px');
            } else {
                $('.main-wrapper').css('padding-top', '0');
            }
        }
        if ($('#wpadminbar').length && $(window).width() < 768) {
            $('#wpadminbar').css({
                'position': 'fixed',
                'top': '0'
            })
        }
    }

    function menuArrows() {
        if (($(window).width() < 1025) || $('.main-menu.modern').length) {
            if (!$('.menu-item-has-children i').length) {
                $('header .menu-item-has-children').append('<i class="fa fa-angle-down"></i>');
                $('header .menu-item-has-children i').addClass('hide-drop');
            }
            $('header .menu-item i').on('click', function () {
                if ($(this).parent().hasClass('menu-item-has-children') && !$(this).hasClass('animation')) {
                    $(this).addClass('animation');
                    if ($(this).hasClass('hide-drop')) {
                        if ($(this).closest('.sub-menu').length) {
                            $(this).removeClass('hide-drop').prev('.sub-menu').slideToggle(400);
                        } else {
                            $('.menu-item-has-children i').addClass('hide-drop').next('.sub-menu').hide(100);
                            $(this).removeClass('hide-drop').prev('.sub-menu').slideToggle(400);
                        }
                    } else {
                        $(this).addClass('hide-drop').prev('.sub-menu').slideToggle(400).find('.menu-item-has-children a').addClass('hide-drop').prev('.sub-menu').slideToggle(400);
                    }
                }
                setTimeout(removeClass, 400);

                function removeClass() {
                    $('header .menu-item i').removeClass('animation');
                }
            });
        } else {
            $('header .menu-item-has-children i').remove();
        }
    }

    /*=================================*/
    /* ANIMATION */
    /*=================================*/

    $.fn.isInViewport = function (offsetB) {

        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height() - offsetB;

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };


    /*=================================*/
    /* HEADER SCROLL */
    /*=================================*/

    $(window).on('load scroll', function () {
        if ($(this).scrollTop() >= 50) {
            if ($('.header_top_bg.header_trans-fixed').length) {
                $('.header_top_bg.header_trans-fixed').not('.fixed-dark').addClass('bg-color-scroll');
                $('.fixed-dark').addClass('bg-fixed-dark');
                $('.logo-hover').show();
                $('.main-logo').hide();
            }
        }
        else {
            if ($('.header_top_bg.header_trans-fixed').length) {
                $('.header_top_bg.header_trans-fixed').not('.fixed-dark').removeClass('bg-color-scroll');
                $('.fixed-dark').removeClass('bg-fixed-dark');
                $('.logo-hover').hide();
                $('.main-logo').show();
            }
        }

        if($(window).width() < 1025 && $('.header_top_bg').length){
            if ($(this).scrollTop() >= 50) {
                if ($('.header_top_bg:not(.header_trans-fixed)').length) {
                    $('.header_top_bg:not(.header_trans-fixed)').not('.fixed-dark').addClass('bg-color-scroll');
                    $('.fixed-dark').addClass('bg-fixed-dark');
                    $('.logo-hover').show();
                    $('.main-logo').hide();
                }
            }
            else {
                if ($('.header_top_bg:not(.header_trans-fixed)').length) {
                    $('.header_top_bg:not(.header_trans-fixed)').not('.fixed-dark').removeClass('bg-color-scroll');
                    $('.fixed-dark').removeClass('bg-fixed-dark');
                    $('.logo-hover').hide();
                    $('.main-logo').show();
                }
            }
        }

    });


    $(".menu > li > .sub-menu").each(function () {

        var $minHeight = $(window).outerHeight() * 0.8;
        var $priceHeight = $(this).height();

        if ($priceHeight > $minHeight) {
            $(this).css({
                "max-height": $(window).outerHeight() * 0.8,
                "overflow-y": "auto"
            })
        }

    });


    /*=================================*/
    /* LIKES FOR BLOG */
    /*=================================*/
    function toggleLikeFromCookies($element, postId) {
        if (document.cookie.search(postId) === -1) {
            $element.removeClass('post__likes--liked');
        } else {
            $element.addClass('post__likes--liked');
        }
    }

    var $likes = $('.post__likes');

    for (var i = 0; i < $likes.length; i++) {
        toggleLikeFromCookies($likes.eq(i), $likes.eq(i).attr('data-id'));
    }

    $likes.on('click', function (e) {
        var $this = $(this),
            post_id = $this.attr('data-id');
        $this.toggleClass('post__likes--liked');
        $this.addClass('post__likes--disable');

        $.ajax({
            type: "POST",
            url: get.ajaxurl,
            data: ({
                action: 'yoy_like_post',
                post_id: post_id
            }),
            success: function (msg) {
                $this.closest('.likes-wrap').find('.count').text(msg);
                toggleLikeFromCookies($this, post_id);
                $this.removeClass('post__likes--disable');
            }
        });
        return false;
    });


    pageCalculations(function () {
        if (!window.enable_foxlazy) {
            wpc_add_img_bg('.s-img-switch');
        }
    });

    function menuAnimation() {
        if ( $('.header_top_bg.animated-menu').length ) {
            $(".header_top_bg.animated-menu:not(.animated)").waypoint(function(){
                var element = $(this),
                    duration = parseInt(element.attr('data-duration')),
                    delay = parseInt(element.attr('data-delay')),
                    animation = element.attr('data-css-animation');

                if ($(this).find('.topmenu .menu > li').length) {
                    $(this).find('.topmenu .menu > li').each(function () {
                        if (!isNaN(duration)) {
                            $(this)
                                .css('animation-duration', duration + 'ms')
                                .css('transition-duration', duration + 'ms')
                        }

                        if (!isNaN(delay)) {
                            $(this)
                                .css('animation-delay', delay + 'ms')
                                .css('transition-delay', delay + 'ms')
                        }

                        delay += 100;
                    });
                }

                element
                    .addClass(animation)
                    .addClass("animated");

            }, {
                offset: "85%"
            });
        }
    }

    function formInputIcon(){
        if($('.wpcf7-form input[name="email"]').length){
            $('.wpcf7-form input[name="email"]').each(function () {
                $(this).closest('span').addClass('input-email');
            })
        }
        if($('.wpcf7-form input[name="name"]').length){
            $('.wpcf7-form input[name="name"]').each(function () {
                $(this).closest('span').addClass('input-name');
            })
        }
        if($('.wpcf7-form input[name="number"]').length){
            $('.wpcf7-form input[name="number"]').each(function () {
                $(this).closest('span').addClass('input-number');
            })
        }
        if($('.wpcf7-form input[name="company"]').length){
            $('.wpcf7-form input[name="company"]').each(function () {
                $(this).closest('span').addClass('input-company');
            })
        }
        if($('.wpcf7-form input[type="submit"]').length){
            $('.wpcf7-form input[type="submit"]').each(function () {
                $(this).wrap('<span class="form-button-wrap"></span>');
            })
        }

        if($('form .input-wrapper input[type="submit"]').length){
            $('form .input-wrapper input[type="submit"]').each(function () {
                $(this).wrap('<span class="form-button-wrap"></span>');
            })
        }
        if($('.search-form form input[type="submit"]').length){
            $('.search-form form input[type="submit"]').each(function () {
                $(this).wrap('<span class="form-button-wrap"></span>');
            })
        }

        if($('.single-content .post-content form input[type="submit"]').length){
            $('.single-content .post-content form input[type="submit"]').each(function () {
                $(this).wrap('<span class="form-button-wrap"></span>');
            })
        }

        if($('.single-content.protected-page form input[type="submit"]').length){
            $('.single-content.protected-page form input[type="submit"]').each(function () {
                $(this).wrap('<span class="form-button-wrap"></span>');
            })
        }
        if($('.wp-block-button__link').length){
            $('.wp-block-button__link').each(function () {
                var text = $(this).text();
                $(this).html('<span>' + text + '</span>');
            })
        }
    }

    /*=================================*/
    /* SHARE SPEAKERS DETAIL */
    /*=================================*/

    $('[data-share]').on('click',function(){

        var w = window,
            url = this.getAttribute('data-share'),
            title = '',
            w_pop = 600,
            h_pop = 600,
            scren_left = w.screenLeft != undefined ? w.screenLeft : screen.left,
            scren_top = w.screenTop != undefined ? w.screenTop : screen.top,
            width = w.innerWidth,
            height = w.innerHeight,
            left = ((width / 2) - (w_pop / 2)) + scren_left,
            top = ((height / 2) - (h_pop / 2)) + scren_top,
            newWindow = w.open(url, title, 'scrollbars=yes, width=' + w_pop + ', height=' + h_pop + ', top=' + top + ', left=' + left);

        if (w.focus) {
            newWindow.focus();
        }

        return false;
    });


    /*=================================*/
    /* LOAD MORE POST LIST*/
    /*=================================*/
    function load_more_posts() {

        // The link of the next page of posts.
        if (window.load_more_post) {
            // The maximum number of pages the current query can return.

            var pageNum = parseInt(load_more_post.startPage) + 1;

            var max = parseInt(load_more_post.maxPage);

            var nextLink = load_more_post.nextLink;

            var wrap_selector = '.fullscreen .main-post-wrap';

            $('.js-load-more').on('click', function (e) {
                var $btn = $(this),
                    $btnText = $btn.html();
                $btn.html('loading...');

                if (pageNum <= max) {

                    var $container = $(wrap_selector);
                    $.ajax({
                        url: nextLink,
                        type: "get",
                        success: function (data) {

                            var newElements = $(data).find('.fullscreen .main-post-wrap .post-wrap-item');

                            var elems = [];

                            newElements.each(function (i) {
                                elems.push(this);
                            });
                            $container.append(elems);
                            $container.find('img[data-lazy-src]').foxlazy();

                            wpc_add_img_bg('.s-img-switch');
                            blogImageSlider();

                            pageNum++;
                            nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/' + pageNum);

                            $btn.html($btnText);

                            if (pageNum == ( max + 1 )) {
                                $btn.hide('fast');
                            }
                        }
                    });
                }
                return false;

            });

        }
    }

    $('.js-load-more').on('click', function(event){
        event.preventDefault();
    });


    $(window).on('load', function () {
        load_more_posts();
        if ( $(window).width() > 1024 ) {
            menuAnimation();
        }
        blogImageSlider();
        popupVideo();
        formInputIcon();
        wpc_add_img_bg('.s-img-switch');
        initSwiper();
    });
    $(window).on('load resize', function () {
        calcPaddingMainWrapper();
        topBannerHeight();
        fixedMobileMenu();
        menuArrows();
        if( $(window).width() > 767 ) {
            initBlogIsotope();
        }
    });
    window.addEventListener("orientationchange", function () {
        calcPaddingMainWrapper();
        topBannerHeight();
        fixedMobileMenu();
        menuArrows();
        $("img[data-lazy-src]").foxlazy('', function () {
            if( $(window).width() > 767 ) {
                initBlogIsotope();
            }
        });
    });
    $(window).on('load scroll resize', function () {
        if($("img[data-lazy-src]").length){
            $("img[data-lazy-src]").foxlazy('', function () {
                if( $(window).width() > 767 ) {
                    initBlogIsotope();
                }
            });
        }
    });


})(jQuery, window, document);