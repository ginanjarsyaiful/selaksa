;(function ($, window, document, undefined) {
    'use strict';


    /*=================================*/
    /* SWIPER SLIDER */
    /*=================================*/
    function initPostSwiper() {
        if ( $('.swiper-container.post-slider').length && $('.swiper-container.img-slider').length ) {

            var swiperImg = new Swiper('.swiper-container.img-slider', {
                slidesPerView: 1,
                loop: true,
                effect: 'fade'
            });

            var swiperPost = new Swiper('.swiper-container.post-slider', {
                slidesPerView: 1,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }
            });

            swiperImg.controller.control = swiperPost;
            swiperPost.controller.control = swiperImg;

        }

    }

    $(window).on('load', function () {
        initPostSwiper();
    });


})(jQuery, window, document);