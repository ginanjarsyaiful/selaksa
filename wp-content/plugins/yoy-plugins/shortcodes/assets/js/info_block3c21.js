;(function ($, window, document, undefined) {
    'use strict';

    if ($('.info-block-wrap.accordeon').length) {
        $('.info-block-wrap.accordeon').each(function () {
            var element = $(this).find('.toggle-list');
            element.on('click', function(e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.next().hasClass('is-show')) {
                    $this.removeClass('active');
                    $this.next().removeClass('is-show');
                    $this.next().slideUp(350);
                } else {
                    $this.parent().parent().find('li .list-drop').removeClass('is-show');
                    $this.parent().parent().find('li .list-drop').slideUp(350);
                    $this.next().toggleClass('is-show');
                    $this.next().slideToggle(350);
                    $this.parent().parent().find('li a').removeClass('active');
                    $this.toggleClass('active');
                }
            });
        });
    }

    if ($('.info-block-wrap.tabs').length) {

        $('.tabs-headers .tabs-headers-item:first-of-type').addClass('active');
        $('.tabs-content .tabs-content-item:first-of-type').addClass('active');
        $('.tabs-headers').on('click', '.tabs-headers-item:not(.active)', function() {
            var index_el = $(this).index();
            $(this).addClass('active').siblings().removeClass('active');
            $(this).closest('.tabs').find('.tabs-content-item').removeClass('active').eq(index_el).addClass('active');
        });

    }


})(jQuery, window, document);