;(function ($, window, document, undefined) {
    'use strict';


    $('.btn-scroll-down').on('click', function () {
        var scroll = $(this).closest('.top-banner').outerHeight() + $(this).closest('.top-banner').offset().top;
        $('html, body').animate({
            scrollTop: scroll
        }, 600);
        return false;
    });

    if ($('.top-banner .images-wrap').length) {

        $('.top-banner').each(function () {

            var items = $(this).find('.images-wrap');

            items.each(function () {
                var id = $(this).attr('id');
                var scene = document.getElementById(id);
                var parallaxInstance = new Parallax(scene, {
                    relativeInput: false,
                    clipRelativeInput: false,
                    calibrationThreshold: 100,
                    calibrationDelay: 500,
                    supportDelay: 500,
                    calibrateX: true,
                    calibrateY: false,
                    invertX: true,
                    invertY: true,
                    limitX: false,
                    limitY: false,
                    scalarX: 5.0,
                    scalarY: 5.0,
                    frictionX: 0.1,
                    frictionY: 0.1,
                    originX: 0.5,
                    originY: 0.5,
                    hoverOnly: true
                });
            });
        });
    }

    function animatedBannerContentPosition() {

        if ($('.top-banner.modern .container').length && $('.top-banner.modern .main-img').length) {
            var wW = $(window).width();
            $('.top-banner.modern .container').each(function () {
                if(wW > 600){
                    var bannerContainer = $(this).height() + 275 + $(this).closest('.top-banner.modern').find('.main-img').height()*0.68;
                    var pT = $(this).closest('.top-banner.modern').find('.main-img').height()*0.68;
                }else{
                    var bannerContainer = $(this).height() + 185 + $(this).closest('.top-banner.modern').find('.main-img').height()*0.68;
                    var pT = $(this).closest('.top-banner.modern').find('.main-img').height()*0.68;
                }

                $(this).closest('.top-banner.modern').find('.bg-wrapper').css('max-height', bannerContainer);

                $(this).closest('.top-banner.modern').find('.main-img').css('margin-top', -pT);

                setTimeout(function (){
                    $('.top-banner.modern .main-img').addClass('active');
                }, 100);

            });
        }

    }


    $(window).on('load resize orientationchange', function () {
        animatedBannerContentPosition();
    })



})(jQuery, window, document);