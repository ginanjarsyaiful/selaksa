;(function ($, window, document, undefined) {
    'use strict';

    $(window).on('load scroll', function() {

        // COUNTER SIMPLE

        if ( $('.counter-wrapper.simple .counter-block-wrap').length) {
            $('.counter-block-item:not(.active)').each(function () {
                if ($(window).scrollTop() >= $(this).offset().top - $(window).height() * 1) {
                    $(this).addClass('active');
                    $(this).find('.counter-block-number').each(function () {
                        $(this).countTo({
                            speed: 500,
                            onComplete: function() {
                                $(this).parent().find('img').addClass('img-show');
                            }
                        });
                    });
                }
            });
        }
    });


})(jQuery, window, document);