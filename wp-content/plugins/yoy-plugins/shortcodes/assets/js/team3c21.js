;(function ($, window, document, undefined) {
    'use strict';


    /*=================================*/
    /* SWIPER SLIDER */
    /*=================================*/
    function initTeamSwiper() {
        if ( $('.swiper-container.team-info-section').length && $('.swiper-container.team-img-section').length ) {

            var swiperImg = new Swiper('.swiper-container.team-img-section', {
                slidesPerView: 1,
                loop: true,
                effect: 'fade',
                autoplay: {
                    delay: 5000
                },
            });

            var swiperInfo = new Swiper('.swiper-container.team-info-section', {
                slidesPerView: 1,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                autoplay: {
                    delay: 5000
                },
            });

            swiperImg.controller.control = swiperInfo;
            swiperInfo.controller.control = swiperImg;

        }

    }

    /*=================================*/
    /* MOUSE ANIMATION */
    /*=================================*/
    function animationMouse(e) {
        if ( $('.team-members-wrap.circle').length ) {
            var mouseY = e.pageY;
            var mouseX = e.pageX;
            var windowCenterX = window.innerWidth / 2;
            var windowCenterY = window.innerHeight / 2;

            $('.js-animation-figure').each(function () {
                var mouseY_item = mouseY - $(this).closest('.js-cs-animation').offset().top;
                var mouseX_item = mouseX;

                var move_x = parseFloat($(this).attr('data-move-x'));
                var move_y = parseFloat($(this).attr('data-move-y'));

                move_x = (mouseX_item / windowCenterX - 1) * move_x * -100;
                move_y = (mouseY_item / windowCenterY - 1) * move_y * -100;

                TweenMax.to($(this), 0.35, {x: move_x, y: move_y, ease: Power2.easeOut});
            });
        }
    }

    document.addEventListener("mousemove", function (event) {
        animationMouse(event);
    });

    $(window).on('load', function () {
        initTeamSwiper();
    });


})(jQuery, window, document);