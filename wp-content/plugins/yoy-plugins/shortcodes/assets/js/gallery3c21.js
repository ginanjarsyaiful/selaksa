;(function ($, window, document, undefined) {
    'use strict';

    $('.justified-wrap-gallery .gallery-wrap').lightGallery({
        selector: '.item',
        mode: 'lg-slide',
        closable: true,
        iframeMaxWidth: '80%',
        download: false,
        thumbnail: true
    });


    function justified_gallery(){
        if($('.justified-wrap-gallery .gallery-wrap').length){
            var image_height = $(window).height() / 2.3;

            if($(window).width() < 1600 ){
                image_height = $(window).height() / 3;
            }

            if($(window).width() <= 1440) {
                image_height = $(window).height() / 2.3;
            }

            if($(window).width() < 1060 ){
                image_height = $(window).height() / 3.5;
            }

            if($(window).width() < 992){
                image_height = $(window).height() / 4.6;
            }

            $('.justified-wrap-gallery .gallery-wrap').justifiedGallery({
                rowHeight : image_height,
                lastRow : 'nojustify',
                margins : 0
            });
        }

    }


    $(window).on('load', function () {
        justified_gallery();
    });

    $(window).on('resize', function () {
        justified_gallery();
    });



})(jQuery, window, document);