;(function ($, window, document, undefined) {
    'use strict';


    /*=================================*/
    /* SWIPER SLIDER */
    /*=================================*/
    function initSpeakersSwiper() {
        if ( $('.swiper-container.speakers').length ) {

            var swiperSpeakers = new Swiper('.swiper-container.speakers', {
                slidesPerView: 3,
                centeredSlides: true,
                loop: true,
                spaceBetween: 40,
                breakpoints: {
                    767: {
                        slidesPerView: 1,
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true
                        },
                    },
                    1600: {
                        spaceBetween: 10,
                        slidesPerView: 2,
                    }
                }
            });

            swiperSpeakers.slideTo(1);
        }
    }

    function initModernSwiper() {
        if ( $('.swiper-container.modern-info').length && $('.swiper-container.modern-img').length ) {

            var swiperInfo = new Swiper('.swiper-container.modern-info', {
                slidesPerView: 1,
                loop: true,
            });

            var swiperImg = new Swiper('.swiper-container.modern-img', {
                slidesPerView: 1,
                loop: true,
                effect: 'fade',
                direction: 'vertical',
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '">' + (index + 1) + '</span>';
                    },
                },
                breakpoints: {
                    767: {
                        allowTouchMove: false,
                        simulateTouch: false
                    },
                }
            });

            swiperInfo.controller.control = swiperImg;
            swiperImg.controller.control = swiperInfo;

        }

    }


    function initSpeakerIsotope() {

        if ($('.isotope_speaker .speaker-wrap').length) {
            $('.isotope_speaker .speaker-wrap').each(function () {
                var self = $(this);
                self.isotope({
                    itemSelector: '.speaker-member',
                    layoutMode: 'masonry',
                    masonry: {
                        columnWidth: '.speaker-member',
                        'gutter': 90
                    }
                });
            });
        }

    }


    $(window).on('load', function () {
        initSpeakersSwiper();
        initModernSwiper();
        initSpeakerIsotope();
    });

    $(window).on('resize', function () {
        initSpeakerIsotope();
    });

    window.addEventListener("orientationchange", function () {
        initSpeakerIsotope();
    });


})(jQuery, window, document);